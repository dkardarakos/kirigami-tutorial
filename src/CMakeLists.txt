set(kirigami-tutorial_SRCS
    main.cpp
    )

qt5_add_resources(RESOURCES resources.qrc)
add_executable(kirigami-tutorial ${kirigami-tutorial_SRCS} ${RESOURCES})
target_link_libraries(kirigami-tutorial Qt5::Core  Qt5::Qml Qt5::Quick Qt5::Svg)
install(TARGETS kirigami-tutorial ${KF5_INSTALL_TARGETS_DEFAULT_ARGS})
